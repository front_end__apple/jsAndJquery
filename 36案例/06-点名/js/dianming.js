var stus = ["李堂宝", "李盘龙", "魏建猛", "宁亚光", "张春杰", "陈卓", "董津亚", "崔云亮", "李宇恒", "刘策",
		"毕军", "李龙", "张喆", "张卓文", "汪帅通", "汪庆文", "魏楚", "赵定富", " 郭昭飞", "白超群",
		"袁松", "南波", "杨为栋", "刘志文", "汪雨瑶", "张军", "张鹏超", "曾若旭", "李旺", "蔺晨辉",
		"申黎明", "梁侃", "王瑞", "李佶", "刘浩", "张骞", "胡浩浩", "张岩", "袁晶", "蔡松林", "冯子乐",
		"刘鹏超", "汪东", "姬文文", "杨文博 ", "荆西盼", "李彦龙", "杜瑞龙", "刘闰通", "杜督"];

var index = -1;
var select = null;

function create() {
	for (var i = 0; i < stus.length; i++) {

		// 使用document对象的createElement函数来创建div元素
		var span = document.createElement("div");

		// 获取父容器的元素
		var par = document.getElementById("par");

		// 给每个div元素设置id属性值
		span.id = "id" + i;

		// 给每个div元素设置背景颜色
		span.style.backgroundColor = "#ff0000";

		// 将每个学生姓名添加到各自div的innerHTML中
		span.innerHTML = stus[i];

		// 给每个div设置class的className
		span.className = "spans"

		// 将每个div元素添加到父容器中
		par.appendChild(span);
	}
}

function startCount1() {
	// 已经有选中的
	if (index != -1) {
		// 获取指定id的元素
		select = document.getElementById("id" + index);

		// 改变已选中的元素的颜色
		select.style.backgroundColor = "#0000ff";
	}

	// 随机获取0-stus.length - 1的数字
	index = parseInt((Math.random() * 1000000) % stus.length);

	// 获取该随机数字对应的元素
	select = document.getElementById("id" + index);

	// 给该元素设置颜色
	select.style.backgroundColor = "#ccff00";
}

// 点击开始按钮
function startCount() {
	// 每隔100毫秒自动调用startCount1()的函数
	id = setInterval(startCount1, 100);
}

// 点击结束按钮
function stopCount() {
	// 清除自动执行功能
	clearInterval(id);

	// 如果有选中元素，则显示学生的姓名
	if (index != -1) {
		alert("点到了" + stus[index]);
	}
}